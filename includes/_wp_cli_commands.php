<?php

if (!class_exists('WP_CLI')) return false;

add_action( 'init', function() {

	WP_CLI::add_command( 'import_players', function( $args ) {

		do_action('download_players_from_MFL');

	    WP_CLI::success('Finished');

	});

});
