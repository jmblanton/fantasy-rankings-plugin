<?php

add_action( 'init', function() {

	$labels = array(
		'name'                       => 'Positions',
		'singular_name'              => 'Position',
		'menu_name'                  => 'Positions',
		'all_items'                  => 'All Positions',
		'parent_item'                => 'Parent Position',
		'parent_item_colon'          => 'Parent Position:',
		'new_item_name'              => 'New Position Name',
		'add_new_item'               => 'Add New Position',
		'edit_item'                  => 'Edit Position',
		'update_item'                => 'Update Position',
		'view_item'                  => 'View Position',
		'separate_items_with_commas' => 'Separate positions with commas',
		'add_or_remove_items'        => 'Add or remove positions',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Positions',
		'search_items'               => 'Search Positions',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No positions',
		'items_list'                 => 'Positions list',
		'items_list_navigation'      => 'Positions list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'position', array( 'players' ), $args );

}, 0 );