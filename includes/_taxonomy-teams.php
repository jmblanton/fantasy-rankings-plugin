<?php

add_action( 'init', function() {

	$labels = array(
		'name'                       => 'Teams',
		'singular_name'              => 'Team',
		'menu_name'                  => 'Teams',
		'all_items'                  => 'All Teams',
		'parent_item'                => 'Parent Team',
		'parent_item_colon'          => 'Parent Team:',
		'new_item_name'              => 'New Team Name',
		'add_new_item'               => 'Add New Team',
		'edit_item'                  => 'Edit Team',
		'update_item'                => 'Update Team',
		'view_item'                  => 'View Team',
		'separate_items_with_commas' => 'Separate team with commas',
		'add_or_remove_items'        => 'Add or remove teams',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Teams',
		'search_items'               => 'Search Teams',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No teams',
		'items_list'                 => 'Teams list',
		'items_list_navigation'      => 'Teams list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => false,
	);
	register_taxonomy( 'teams', array( 'players' ), $args );

}, 0 );