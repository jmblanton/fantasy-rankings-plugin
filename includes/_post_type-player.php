<?php

add_action( 'init', function() {

	$labels = array(
		'name'                  => 'Players',
		'singular_name'         => 'Player',
		'menu_name'             => 'Players',
		'name_admin_bar'        => 'Player',
		'archives'              => 'Player Archives',
		'attributes'            => 'Player Attributes',
		'parent_item_colon'     => 'Parent Player:',
		'all_items'             => 'All Players',
		'add_new_item'          => 'Add New Player',
		'add_new'               => 'Add New',
		'new_item'              => 'New Player',
		'edit_item'             => 'Edit Player',
		'update_item'           => 'Update Player',
		'view_item'             => 'View Player',
		'view_items'            => 'View Players',
		'search_items'          => 'Search Player',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into player',
		'uploaded_to_this_item' => 'Uploaded to this player',
		'items_list'            => 'Players list',
		'items_list_navigation' => 'Players list navigation',
		'filter_items_list'     => 'Filter players list',
	);
	$args = array(
		'label'                 => 'Player',
		'description'           => 'Players imported from MFL',
		'labels'                => $labels,
		'supports'              => array( 'title', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'players', $args );

}, 0 );