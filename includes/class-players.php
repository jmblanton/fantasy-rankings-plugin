<?php

class Players {

	public $raw_data;

	public $parsed_data;

	public $version;

	public $timestamp;

	public $players;

	public $wp_user_id;


	public function __construct($url) {

		$cache = glob(dirname(__DIR__) .'/cache/*.json');

		if(!empty($cache)) {

			$cache_dates = array_map( function($file) { return pathinfo($file)['filename']; }, $cache );

			$this->raw_data = file_get_contents(dirname(__DIR__) . '/cache/' . max($cache_dates) . '.json');

		}

		else {

			$this->raw_data = file_get_contents($url);

		}

		if(empty($this->raw_data)) return false;

		$this->parsed_data = json_decode($this->raw_data, true);

		$this->version = $this->parsed_data['version'];

		$this->timestamp = $this->parsed_data['players']['timestamp'];

		$this->players = $this->parsed_data['players']['player'];

		$this->wp_user_id = 2;

	}

	public function get_meta_keys() {

		$keys = array();

		foreach ($this->players as $player) $keys = array_merge(array_keys($player), $keys);

		return array_values(array_unique($keys));

	}


	public function get_teams() {

		$teams = array();

		foreach ($this->players as $player) $teams[] = $player['team'];

		return array_values(array_unique($teams));

	}


	public function get_positions() {

		$positions = array();

		foreach ($this->players as $player) $positions[] = $player['position'];

		return array_values(array_unique($positions));

	}


	public function import_teams() {

		$teams = $this->get_teams();

		if(!empty($teams)) foreach ($teams as $team) if(!term_exists( $team, 'teams' )) wp_insert_term( $team, 'teams' );

		return true;

	}


	public function import_positions() {

		$positions = $this->get_positions();

		if(!empty($positions)) foreach ($positions as $position) if(!term_exists( $position, 'position' )) wp_insert_term( $position, 'position' );

		return true;

	}

	public function import_players() {

		foreach ($this->players as $player) {

			$meta_key_array = array();

			foreach ($player as $meta_key => $meta_value) {

				if($meta_key == 'name' || $meta_key == 'position' || $meta_key == 'team') continue;

				if($meta_key == 'id') {

					$meta_key_array['mfl_id'] = $meta_value;

					continue;

				}

				$meta_key_array[$meta_key] = $meta_value;

			}

			$wp_player_query = new WP_Query(array(
				'post_type'  => 'players',
				'meta_key'   => 'mfl_id',
				'meta_value' => $player['id'],
				'meta_compare' => '=', 
			));

			if($wp_player_query->post_count == 0) {

				$id = wp_insert_post(array(
					'post_author' => $this->wp_user_id,
					'post_title'  => $player['name'],
					'post_type'   => 'players',
					'post_status' => 'publish',
					'meta_input'  => $meta_key_array
				));

				wp_set_post_terms( $id, $player['team'], 'teams', false );

				wp_set_post_terms( $id, $player['position'], 'position', false );
				
			}

			else {

				foreach ($meta_key_array as $meta_key => $meta_value) update_post_meta($wp_player_query->post->ID, $meta_key, $meta_value);

				wp_set_post_terms( $wp_player_query->post->ID, $player['team'], 'teams', false );

				wp_set_post_terms( $wp_player_query->post->ID, $player['position'], 'position', false );

			}
	
		}

		return true;

	}

}